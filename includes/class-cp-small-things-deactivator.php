<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://spacewax.net
 * @since      1.0.0
 *
 * @package    Cp_Small_Things
 * @subpackage Cp_Small_Things/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Cp_Small_Things
 * @subpackage Cp_Small_Things/includes
 * @author     JMColeman <hyperclock@spacewax.net>
 */
class Cp_Small_Things_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
