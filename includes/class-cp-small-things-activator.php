<?php

/**
 * Fired during plugin activation
 *
 * @link       https://spacewax.net
 * @since      1.0.0
 *
 * @package    Cp_Small_Things
 * @subpackage Cp_Small_Things/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Cp_Small_Things
 * @subpackage Cp_Small_Things/includes
 * @author     JMColeman <hyperclock@spacewax.net>
 */
class Cp_Small_Things_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
