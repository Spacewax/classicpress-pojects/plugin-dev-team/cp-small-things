<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://spacewax.net
 * @since      1.0.0
 *
 * @package    Cp_Small_Things
 * @subpackage Cp_Small_Things/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Cp_Small_Things
 * @subpackage Cp_Small_Things/includes
 * @author     JMColeman <hyperclock@spacewax.net>
 */
class Cp_Small_Things_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'cp-small-things',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
