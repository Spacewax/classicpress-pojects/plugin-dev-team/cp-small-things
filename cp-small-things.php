<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://spacewax.net
 * @since             1.0.0
 * @package           Cp_Small_Things
 *
 * @wordpress-plugin
 * Plugin Name:       CP Small Things
 * Plugin URI:        https://gitlab.com/Spacewax/classicpress-pojects/plugin-dev-team/cp-small-things
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            JMColeman
 * Author URI:        https://spacewax.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cp-small-things
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cp-small-things-activator.php
 */
function activate_cp_small_things() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cp-small-things-activator.php';
	Cp_Small_Things_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cp-small-things-deactivator.php
 */
function deactivate_cp_small_things() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cp-small-things-deactivator.php';
	Cp_Small_Things_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_cp_small_things' );
register_deactivation_hook( __FILE__, 'deactivate_cp_small_things' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cp-small-things.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_cp_small_things() {

	$plugin = new Cp_Small_Things();
	$plugin->run();

}
run_cp_small_things();
